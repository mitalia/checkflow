from PyQt4.Qt import *
import random

a = QApplication([])

ri = random.randint

class CheckWidget(QWidget):
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)
        self.ch = [QCheckBox("%d x %d" % (ri(1, 99), ri(1, 99)), self) for _ in range(1000)]
        self.tm = QTimer(self)
        self.tm.setInterval(100)
        self.tm.timeout.connect(self.moveStuff)
        self.tm.start()
        self.count = 0

    def moveStuff(self):
        self.count += 1
        w = self.width()
        h = self.height()
        flip = self.count % 30 == 0
        for ch in self.ch:
            x = ch.x()
            y = ch.y()
            x += ri(-2, +27)
            y += ri(-5, +5)
            while x >= w:
                x -= w
            while y >= h:
                y -= h
            ch.move(x, y)
            if flip:
                ch.setChecked(not ch.isChecked())

    def resizeEvent(self, ev):
        for ch in self.ch:
            ch.move(ri(0,self.width()), ri(0,self.height()))
            ch.setChecked(ri(0, 1))

cw = CheckWidget()
cw.resize(800,600)
cw.show()

a.exec_()
